from easy_thumbnails.fields import ThumbnailerImageField
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class Employee(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'employers'
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'ФИО')
    image = ThumbnailerImageField(upload_to='employers/', blank=False, null=False, verbose_name=u'Изображение новости',
                                  resize_source=dict(size=(140, 140), crop=True))
    content = RichTextUploadingField(null=False, blank=False, verbose_name=u'Описание')
    position = models.IntegerField(verbose_name=u'Позиция', default=0)
