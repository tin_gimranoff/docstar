from django.shortcuts import render
from employee.models import Employee

def index(request):
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Наша команда',
        'link': '/employers/',
        'active': False
    }]
    employee = Employee.objects.order_by('position').all()
    meta_tags = {
        'title': u'Наша команда',
        'keywords': u'сотрудники, врачи, доктора',
        'description': u'Все сотрудники клиники DocStar',
    }
    return render(request, 'employee/index.html', {
        'breadcrumbs': breadcrumbs,
        'employers': employee,
        'meta_tags': meta_tags,
    })
