from django import template
from mainpage.models import SiteSettings

register = template.Library()


@register.filter
def get_site_setting(value):
    settings = SiteSettings.objects.get()
    return getattr(settings, value)
