from django import template

from mainpage.models import Menu

register = template.Library()


@register.inclusion_tag('menu/_header_meu.html')
def get_header_menu(request):
    menu = []
    parent_menu = Menu.objects.filter(parent=None).all()
    for p in parent_menu:
        children_menu_arr = []
        children_menu = Menu.objects.filter(parent=p.id)
        if len(children_menu) > 0:
            for c in children_menu:
                children_menu_arr.append({
                    'name': c.name,
                    'link': c.link,
                })
        menu.append({
            'name': p.name,
            'link': p.link,
            'children': children_menu_arr if len(children_menu_arr) > 0 else None,
            'active': 'active' if request.path == p.link else ''
        })
    return {
        'menu': menu
    }


@register.inclusion_tag('menu/_footer_menu.html')
def get_footer_menu():
    menu = Menu.objects.filter(parent=None).all()
    return {
        'menu': menu
    }
