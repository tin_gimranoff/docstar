from django.contrib import admin
from mainpage.models import SiteSettings, Menu


class MenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'parent')


admin.site.register(Menu, MenuAdmin)
admin.site.register(SiteSettings)
