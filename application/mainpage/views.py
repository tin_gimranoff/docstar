from django.shortcuts import render

from mainpage.models import SiteSettings
from textpage.models import Textpage


def index(request):
    page_for_main = Textpage.objects.get(onMain=True)
    settings = SiteSettings.objects.get(id=1)
    print('123')
    meta_tags = {
        'title': u'Клиника DocStar',
        'keywords': settings.meta_keywords,
        'description': settings.meta_description,
    }
    return render(request, 'mainpage/index.html', {
        'page_for_main': page_for_main,
        'meta_tags': meta_tags,
    })

def handler404(request, exception, template_name='404.html'):
    respone = render(request, template_name)
    respone.status_code = 404
    return respone
