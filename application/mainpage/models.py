from django.db import models


class SiteSettings(models.Model):
    def __str__(self):
        return u'Настройки сайта'

    class Meta:
        db_table = 'site_settings'
        verbose_name = u'Настройки сайта'
        verbose_name_plural = u'Настройки сайта'

    logo = models.ImageField(verbose_name=u'Логотип')
    site_name = models.CharField(max_length=255, verbose_name='Название сайта')
    meta_keywords = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Ключевые слова')
    meta_description = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Описание')
    phones = models.CharField(max_length=255, verbose_name=u'Номера телефонов')
    timing = models.CharField(max_length=255, verbose_name=u'Расписание в шапке')
    address = models.CharField(max_length=255, verbose_name=u'Адрес')
    email = models.EmailField(max_length=255, verbose_name='Email')
    bank_data = models.TextField(null=True, blank=True, verbose_name=u'Реквизиты')
    map_link = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Ссылка на карту')


class Menu(models.Model):
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'menus'
        verbose_name = u'Элемент меню'
        verbose_name_plural = u'Элемнеты меню'
        ordering = ['position']

    name = models.CharField(max_length=255, verbose_name=u'Название', null=False, blank=False)
    link = models.CharField(max_length=255, verbose_name=u'Ссылка', null=False, blank=False)
    parent = models.ForeignKey("self", verbose_name=u'Родительский элемент', default=None, blank=True, null=True,
                               on_delete=None)
    position = models.IntegerField(default=0, verbose_name=u'Позиция')
