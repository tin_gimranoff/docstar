"""docstar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

from mainpage import views as mainpage_views
from textpage.models import Textpage
from textpage import views as textpages_views

pages = Textpage.objects.all()
pages_alias = [p.alias for p in pages]
pages_alias = "|".join(pages_alias)

urlpatterns = [
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('admin/', admin.site.urls),
    path('', mainpage_views.index),
    path('surprices/', include('surprice.urls')),
    path('news/', include('news.urls')),
    path('employers/', include('employee.urls')),
    re_path(r'^gallery/', include('photologue_custom.urls', namespace='photologue')),
    path('reviews/', include('reviews.urls')),
    path('contacts/', include('contacts.urls')),
    path('sitemap.xml', include('sitemap.urls')),
    re_path(r'^(?P<page_alias>(' + pages_alias + '))/', textpages_views.index),
]

handler404 = 'mainpage.views.handler404'
