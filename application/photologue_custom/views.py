from django.shortcuts import render, get_object_or_404
from photologue.models import Photo, Gallery

from photologue_custom.models import GalleryExtended


def albums(request):
    albums = GalleryExtended.objects.all()
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Фотоальбомы',
        'link': '/gallery/',
        'active': False
    }]
    meta_tags = {
        'title': u'Фотоальбомы',
        'keywords': u'фотоальбомы',
        'description': u'Все фотоальбомы клиники DocStar',
    }
    return render(request, 'albums/albums.html', {
        'breadcrumbs': breadcrumbs,
        'meta_tags': meta_tags,
        'albums': albums,
    })

def album(request, slug):
    album = get_object_or_404(Gallery, slug=slug)
    album_extended = GalleryExtended.objects.get(gallery_id=album.id)
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Фотоальбомы',
        'link': '/gallery/',
        'active': True
    }, {
        'title': album.title,
        'link': '/gallery/%s/' % album.slug,
        'active': False
    }]
    meta_tags = {
        'title': u'%s — Фотоальбомы' % album.title,
        'keywords': album_extended.meta_keywords,
        'description': album_extended.meta_description,
    }
    return render(request, 'albums/album.html', {
        'breadcrumbs': breadcrumbs,
        'meta_tags': meta_tags,
        'album': album,
        'photos': album.photos.all(),
    })