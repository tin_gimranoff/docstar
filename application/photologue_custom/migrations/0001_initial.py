# Generated by Django 2.0.1 on 2018-02-05 17:48

from django.db import migrations, models
import easy_thumbnails.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('photologue', '0010_auto_20160105_1307'),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryExtended',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meta_keywords', models.CharField(blank=True, max_length=255, verbose_name='Ключевые слова')),
                ('meta_description', models.CharField(blank=True, max_length=255, verbose_name='Описание')),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(upload_to='surprice/', verbose_name='Обложка альбома')),
                ('gallery', models.OneToOneField(on_delete=None, related_name='extended', to='photologue.Gallery')),
            ],
            options={
                'verbose_name': 'Дополнительные поля',
                'verbose_name_plural': 'Дополнительные поля',
            },
        ),
    ]
