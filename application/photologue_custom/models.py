from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from photologue.models import Gallery


class GalleryExtended(models.Model):
    gallery = models.OneToOneField(Gallery, related_name='extended', on_delete=models.CASCADE)

    class Meta:
        verbose_name = u'Дополнительные поля'
        verbose_name_plural = u'Дополнительные поля'

    meta_keywords = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Ключевые слова')
    meta_description = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Описание')
    image = ThumbnailerImageField(upload_to='albums/', blank=False, null=False, verbose_name=u'Обложка альбома',
                                  resize_source=dict(size=(495, 300), crop=True))

    def __str__(self):
        return self.gallery.title
