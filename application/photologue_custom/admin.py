from django.contrib import admin
from photologue.admin import GalleryAdmin as GalleryAdminDefault
from photologue.models import Watermark, PhotoEffect, Gallery
from .models import GalleryExtended

class GalleryExtendedInline(admin.StackedInline):
    model = GalleryExtended
    can_delete = False

class GalleryAdmin(GalleryAdminDefault):
    inlines = [GalleryExtendedInline, ]

admin.site.unregister(Watermark)
admin.site.unregister(PhotoEffect)
admin.site.unregister(Gallery)
admin.site.register(Gallery, GalleryAdmin)


