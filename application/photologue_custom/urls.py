from django.conf.urls import url
from photologue.views import PhotoDetailView

from photologue_custom.views import albums, album

"""NOTE: the url names are changing. In the long term, I want to remove the 'pl-'
prefix on all urls, and instead rely on an application namespace 'photologue'.

At the same time, I want to change some URL patterns, e.g. for pagination. Changing the urls
twice within a few releases, could be confusing, so instead I am updating URLs bit by bit.

The new style will coexist with the existing 'pl-' prefix for a couple of releases.

"""


app_name = 'photologue'
urlpatterns = [
    url(r'^$', albums, name='pl-photologue-root'),
    url(r'^(?P<slug>[\-\d\w]+)/$', album, name='pl-gallery'),
    url(r'^photo/(?P<slug>[\-\d\w]+)/$', PhotoDetailView.as_view(), name='pl-photo'),
]
