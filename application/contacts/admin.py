from django.contrib import admin
from contacts.models import Administrator

admin.site.register(Administrator)
