from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class Administrator(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'administartors'
        verbose_name = u'Администратор'
        verbose_name_plural = u'Администраторы'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'ФИО')
    image = ThumbnailerImageField(upload_to='administrators/', blank=False, null=False, verbose_name=u'Изображение',
                                  resize_source=dict(size=(140, 140), crop=True))
    rank = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Должность')
    phone = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Телефон')
    email = models.EmailField(max_length=255, blank=False, null=False, verbose_name=u'Email')