from django.shortcuts import render

from contacts.models import Administrator
from mainpage.models import SiteSettings


def contacts(request):
    settings = SiteSettings.objects.get(id=1)
    administrators = Administrator.objects.all()
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Контакты',
        'link': '/contacts/',
        'active': False
    }]
    meta_tags = {
        'title': u'Контакты',
        'keywords': u'контакты, телефоны регистратуры, админисрация клиники',
        'description': u'Контакты клиники DocStar',
    }
    return render(request, 'contacts/contacts.html', {
        'site_settings': settings,
        'administrators': administrators,
        'breadcrumbs': breadcrumbs,
        'meta_tags': meta_tags,
    })
