from django.contrib import admin
from reviews.models import Review

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('date_added', 'show_on_site', 'email')

admin.site.register(Review, ReviewAdmin)
