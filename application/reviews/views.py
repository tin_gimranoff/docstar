import datetime

from django.contrib import messages
from django.shortcuts import render

from reviews.models import Review
from .forms import ReviewForm

import requests

def index(request):
    #detect ip
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    url = "http://api.stopforumspam.org/api"

    form = ReviewForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            review_form = form.save(commit=False)
            review_form.date_added = datetime.datetime.now()
            #check in stop forumspam
            response = requests.get(url, params={
                'email': review_form.email,
                'ip': ip,
                'f': 'json',
            })
            if response.status_code == 200:
                response = response.json()
                if response['ip']['appears'] == 0 and response['email']['appears'] == 0:
                    review_form.save()
                    messages.add_message(request, messages.SUCCESS,
                                         'Ваш отзыв успешно отправлен. Нам очень важно ваше мнение.')
    reviews = Review.objects.order_by('-date_added').filter(show_on_site=True).all()
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Отзывы',
        'link': '/reviews/',
        'active': False
    }]
    meta_tags = {
        'title': u'Отзывы',
        'keywords': u'отзывы',
        'description': u'Все отзывы клиники DocStar',
    }
    return render(request, 'reviews/index.html', {
        'form': form,
        'reviews': reviews,
        'breadcrumbs': breadcrumbs,
        'meta_tags': meta_tags
    })
