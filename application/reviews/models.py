from django.db import models


class Review(models.Model):

    def __str__(self):
        return str(self.date_added)

    class Meta:
        db_table = 'reviews'
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'

    email = models.EmailField(blank=True, verbose_name='Email (не публикуется)')
    date_added = models.DateTimeField(blank=False, null=False, verbose_name='Дата добавления')
    text = models.TextField(blank=False, null=False, verbose_name='Содержание отзыва')
    show_on_site = models.BooleanField(default=False, verbose_name='Показывать на сайте')
    image = models.ImageField(upload_to='reviews/', null=True, blank=True, verbose_name=u'Фото')