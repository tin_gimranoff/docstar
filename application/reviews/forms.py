from django import forms

from reviews.models import Review


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            'email',
            'text',
            'image'
        ]
        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email (не публикуется)',
                'required': True
            }),
            'text': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': '8',
                'cols': '80',
                'placeholder': 'Опишите, что вам понравилось или может не понравилось...',
            })
        }