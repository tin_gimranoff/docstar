#!/usr/bin/env python
import os
import sys
import time
import MySQLdb

def wait_for_db():
    try:
        db = MySQLdb.connect(
            host=os.environ.get('HOST'),
            user=os.environ.get('MYSQL_USER'),
            passwd=os.environ.get('MYSQL_PASSWORD'),
            db=os.environ.get('MYSQL_DATABASE')
        )
        db.close()
        return True
    except Exception as e:
        print("DB Exception: ", e)
        return False


if __name__ == '__main__':
    while not wait_for_db():
        print("Waiting for db connection...")
        time.sleep(5)
    print("DB connection OK")
    cmd = sys.argv[1]
    env = os.environ.copy()
    args = sys.argv[1:]
    print('Launch known command "{}"'.format(cmd))
    os.execvpe(cmd, args, env)
