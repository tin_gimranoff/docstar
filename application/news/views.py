from django.shortcuts import render, get_object_or_404
from news.models import News


def index(request):
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Новости',
        'link': '/news/',
        'active': False
    }]
    news = News.objects.order_by('-date_start').all()
    meta_tags = {
        'title': u'Новости',
        'keywords': u'новости',
        'description': u'Все новости клиники DocStar',
    }
    return render(request, 'news/index.html', {
        'breadcrumbs': breadcrumbs,
        'news': news,
        'meta_tags': meta_tags,
    })


def view(request, alias):
    news = get_object_or_404(News, alias=alias)
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Новости',
        'link': '/news/',
        'active': True
    }, {
        'title': news.title,
        'link': '/news/%s/' % news.alias,
        'active': False,
    }]
    meta_tags = {
        'title': u'%s — Новости' % news.title,
        'keywords': news.meta_keywords,
        'description': news.meta_description,
    }
    return render(request, 'news/view.html', {
        'breadcrumbs': breadcrumbs,
        'news': news,
        'meta_tags': meta_tags
    })
