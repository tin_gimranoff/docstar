from django import template
from news.models import News
from docstar.settings import NEWS_ON_PAGE

register = template.Library()


@register.inclusion_tag('news/_sidebar_news.html')
def get_sidebar_news():
    news = News.objects.order_by('-date_start')[:NEWS_ON_PAGE]
    return {
        'news': news
    }


@register.inclusion_tag('news/_bottom_news.html')
def get_bottom_news():
    news = News.objects.order_by('-date_start')[:NEWS_ON_PAGE]
    return {
        'news': news
    }
