from django.shortcuts import render, get_object_or_404
from surprice.models import Surprice


def index(request):
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Все акции',
        'link': '/surprices/',
        'active': False
    }]
    surprices = Surprice.objects.order_by('-date_start').all()
    meta_tags = {
        'title': u'Акции',
        'keywords': u'акции, скидки',
        'description': u'Все акции и скидки в клинике DocStar',
    }
    return render(request, 'surprice/index.html', {
        'breadcrumbs': breadcrumbs,
        'surprices': surprices,
        'meta_tags': meta_tags,
    })


def view(request, alias):
    surprice = get_object_or_404(Surprice, alias=alias)
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }, {
        'title': u'Акции',
        'link': '/surprices/',
        'active': True
    }, {
        'title': surprice.title,
        'link': '/surprices/%s/' % surprice.alias,
        'active': False,
    }]
    meta_tags = {
        'title': u'%s — Акции' % surprice.title,
        'keywords': surprice.meta_keywords,
        'description': surprice.meta_description,
    }
    return render(request, 'surprice/view.html', {
        'breadcrumbs': breadcrumbs,
        'surprice': surprice,
        'meta_tags': meta_tags
    })
