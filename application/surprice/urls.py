from django.urls import path

from surprice import views

urlpatterns = [
    path('', views.index),
    path('<str:alias>/', views.view)
]
