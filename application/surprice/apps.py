from django.apps import AppConfig


class SurpriceConfig(AppConfig):
    name = 'surprice'
    verbose_name = u'Акции'
