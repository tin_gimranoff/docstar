from easy_thumbnails.fields import ThumbnailerImageField
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class Surprice(models.Model):

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'surprices'
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'

    title = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Заголовок')
    meta_keywords = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Ключевые слова')
    meta_description = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Описание')
    date_start = models.DateField(blank=False, null=False, verbose_name=u'Дата начала акции')
    alias = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'URL')
    image = ThumbnailerImageField(upload_to='surprice/', blank=False, null=False, verbose_name=u'Изображение акции',
                                  resize_source=dict(size=(495, 300), crop=True))
    #image = models.ImageField(upload_to='surprice/', blank=False, null=False, verbose_name=u'Изображение акции', )
    intro = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Краткое содержание')
    content = RichTextUploadingField(null=False, blank=False, verbose_name=u'Содержание')
