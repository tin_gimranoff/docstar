from django import template
from surprice.models import Surprice
from docstar.settings import SURPRICE_ON_PAGE

register = template.Library()


@register.inclusion_tag('surprice/_sidebar_surprice.html')
def get_sidebar_surprice():
    surprices = Surprice.objects.order_by('-date_start')[:SURPRICE_ON_PAGE]
    return {
        'surprices': surprices
    }


@register.inclusion_tag('surprice/_bottom_surprice.html')
def get_bottom_surprice():
    surprices = Surprice.objects.order_by('-date_start')[:SURPRICE_ON_PAGE]
    return {
        'surprices': surprices
    }
