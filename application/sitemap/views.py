from django.shortcuts import render
from photologue.models import Gallery
from surprice.models import Surprice
from news.models import News

from textpage.models import Textpage


def sitemap(request):
    urls = []
    server_name = request.META['SERVER_NAME']
    urls.append({
        'loc': 'http://%s' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    urls.append({
        'loc': 'http://%s/contacts/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    urls.append({
        'loc': 'http://%s/employers/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    urls.append({
        'loc': 'http://%s/gallery/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    albums = Gallery.objects.all()
    for a in albums:
        urls.append({
            'loc': 'http://%s/gallery/%s/' % (server_name, a.slug),
            'changefreq': 'monthly',
            'priority': '0.8'
        })
    urls.append({
        'loc': 'http://%s/surprices/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    surprices = Surprice.objects.all()
    for s in surprices:
        urls.append({
            'loc': 'http://%s/surprices/%s/' % (server_name, s.alias),
            'changefreq': 'monthly',
            'priority': '0.8'
        })
    urls.append({
        'loc': 'http://%s/news/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    news = News.objects.all()
    for n in news:
        urls.append({
            'loc': 'http://%s/news/%s/' % (server_name, n.alias),
            'changefreq': 'monthly',
            'priority': '0.8'
        })
    urls.append({
        'loc': 'http://%s/reviews/' % (server_name),
        'changefreq': 'monthly',
        'priority': '0.8'
    })
    textpages = Textpage.objects.all()
    for t in textpages:
        if t.alias != '#':
            urls.append({
                'loc': 'http://%s/%s/' % (server_name, t.alias),
                'changefreq': 'monthly',
                'priority': '0.8'
            })
    return render(request, 'sitemap.xml', {
        'urls': urls,
    }, content_type='text/xml')
