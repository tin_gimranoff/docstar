from django.contrib import admin
from textpage.models import Textpage


class TextpageAdmin(admin.ModelAdmin):
    list_display = ('title', 'alias')


admin.site.register(Textpage, TextpageAdmin)
