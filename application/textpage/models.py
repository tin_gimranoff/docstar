from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models


class Textpage(models.Model):

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'textpages'
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'

    title = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Заголовок')
    meta_keywords = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Ключевые слова')
    meta_description = models.CharField(max_length=255, blank=True, null=False, verbose_name=u'Описание')
    alias = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'URL')
    content = RichTextUploadingField(null=False, blank=False, verbose_name=u'Содержание')
    onMain = models.BooleanField(default=False, verbose_name=u'Показывать на главной')
