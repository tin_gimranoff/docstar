from django.shortcuts import render

from mainpage.models import Menu
from textpage.models import Textpage


def index(request, page_alias):
    page = Textpage.objects.get(alias=page_alias)
    breadcrumbs = [{
        'title': u'Главная',
        'link': '/',
        'active': True
    }]
    menu_element = Menu.objects.get(link='/%s/' % page_alias)
    if menu_element.parent_id is not None:
        parent_menu = Menu.objects.get(id=menu_element.parent_id)
        if parent_menu.link != '#':
            active = True
        else:
            active = False
        breadcrumbs.append({
            'title': parent_menu.name,
            'link': parent_menu.link,
            'active': active
        })
    breadcrumbs.append({
        'title': menu_element.name,
        'link': menu_element.link,
        'active': False,
    })
    meta_tags = {
        'title': page.title,
        'keywords': page.meta_keywords,
        'description': page.meta_description,
    }
    return render(request, 'textpage/index.html', {
        'page': page,
        'breadcrumbs': breadcrumbs,
        'meta_tags': meta_tags,
    })
